<?php
namespace Charm\Terminal\Parser;

use Charm\Terminal;

class PhpHighlighter {

    private Terminal $out;

    public function __construct(Terminal $out) {
        $this->out = $out;
    }

    public function __invoke(string $buffer) {
        $result = '';
        foreach (token_get_all($buffer) as $token) {
            switch ($token[0]) {
                case \T_OPEN_TAG:
                case \T_OPEN_TAG_WITH_ECHO:
                case \T_CLOSE_TAG:
                    $result .= '<!lime>'.$token[1].'<!>';
                    break;
                case \T_WHITESPACE:
                    $result .= $token[1];
                    break;
                case \T_CLASS:                                      // class, abstract, case, switch, try, catch
                case \T_ABSTRACT:
                case \T_BREAK:
                case \T_CALLABLE:
                case \T_CLONE:
                case \T_CONST:
                case \T_CONTINUE:
                case T_DECLARE:
                case T_DEFAULT:
                case T_DO:
                case T_ECHO:
                case T_ELSE:
                case T_ELSEIF:
                case T_EMPTY:
                case T_ENDDECLARE:
                case T_ENDFOR:
                case T_ENDFOREACH:
                case T_ENDIF:
                case T_ENDSWITCH:
                case T_ENDWHILE:
                case T_ENUM:
                case T_EVAL:
                case T_EXIT:
                case T_EXTENDS:
                case T_FINAL:
                case T_FINALLY:
                case T_FN:
                case T_FOR:
                case T_FOREACH:
                case T_FUNCTION:
                case T_GLOBAL:
                case T_GOTO:
                case T_HALT_COMPILER:
                case T_IF:
                case T_IMPLEMENTS:
                case T_INCLUDE:
                case T_INCLUDE_ONCE:
                case T_INTERFACE:
                case T_ISSET:
                case T_LIST:
                case T_MATCH:
                case T_NEW:
                case T_PRINT:
                case T_PRIVATE:
                case T_PROTECTED:
                case T_PUBLIC:
                case T_READONLY:
                case T_REQUIRE:
                case T_REQUIRE_ONCE:
                case T_RETURN:
                case T_STATIC:
                case T_SWITCH:
                case T_THROW:
                case T_TRAIT:
                case T_TRY:
                case T_UNSET:
                case T_UNSET_CAST:
                case T_USE:
                case T_VAR:
                case T_WHILE:
                case T_YIELD:
                case T_YIELD_FROM:
                    $result .= "<!aqua>".$token[1]."<!>";
                    break;
                case \T_REQUIRE:                                    // LANGUAGE CONSTRUCTS
                    $result .= "<!white>".$token[1]."<!>";
                    break;
                case \T_DIR:                                        // MAGIC CONSTANTS
                case \T_CLASS_C:
                case \T_FILE:
                case \T_FUNC_C:
                case \T_LINE:
                case \T_METHOD_C:
                case \T_NS_C:
                    $result .= "<!blue>".$token[1]."<!>";
                    break;
                case \T_NS_SEPARATOR:                               // LITERAL SEPARATORS
                case \T_DOUBLE_COLON:
                case T_PAAMAYIM_NEKUDOTAYIM:
                    $result .= "<!aqua>".$token[1]."<!>";
                    break;
                case '.':
                    $result .= "<!teal>".$token."<!>";
                    break;
                case \T_CONSTANT_ENCAPSED_STRING:                   // LITERALS
                    $result .= "<!yellow>".$token[1]."<!>";
                    break;
                case \T_STRING:
                case \T_NAMESPACE:
                case \T_NAME_FULLY_QUALIFIED:
                case \T_NAME_QUALIFIED:
                case \T_NAME_RELATIVE:
                    if (!function_exists($token[1])) {
                        $result .= "<!lime>".$token[1]."<!>";
                    } else {
                        $result .= "<!silver>".$token[1]."<!>";
                    }
                    break;
                case \T_COMMENT:                                    // COMMENTS
                    $result .= "<!yellow italic>".$token[1]."<!>";
                    break;
                case \T_AND_EQUAL:                                  // ASSIGNMENT OPERATORS
                case \T_COALESCE_EQUAL:
                case \T_CONCAT_EQUAL:
                case \T_DIV_EQUAL:
                case \T_MINUS_EQUAL:
                case \T_MOD_EQUAL:
                case \T_MUL_EQUAL:
                case \T_OR_EQUAL:
                case \T_PLUS_EQUAL:
                case \T_POW_EQUAL:
                case \T_SL_EQUAL:
                case \T_SR_EQUAL:
                case \T_XOR_EQUAL:
                    $result .= "<!yellow>".$token."<!>";
                    break;
                case \T_BOOLEAN_AND:                                // BINARY OPERATORS
                case \T_BOOLEAN_OR:
                case \T_COALESCE:
                case \T_INSTANCEOF:
                case \T_INSTEADOF:
                case \T_IS_EQUAL:
                case \T_IS_GREATER_OR_EQUAL:
                case \T_IS_IDENTICAL:
                case \T_IS_NOT_EQUAL:
                case \T_IS_NOT_IDENTICAL:
                case \T_IS_SMALLER_OR_EQUAL:
                case \T_LOGICAL_AND:
                case \T_LOGICAL_OR:
                case \T_LOGICAL_XOR:
                case \T_POW:
                case \T_SL:
                case \T_SPACESHIP:
                case \T_SR:
                case \T_DEC:
                case \T_INC:
                    $result .= "<!white>".$token[1]."<!>";
                    break;
                case '(': case ')':                                 // STRUCTURAL
                case '[': case ']':
                case '{': case '}':
                case ',': case ';':
                    $result .= "<!silver>".$token."<!>";
                    break;
                default:
                    if (is_array($token) && function_exists($token[1])) {
                        $result .= "<!silver>".$token[1]."<!>";
                        continue;
                    }
                    if (is_array($token)) {
                        if (\function_exists($token[1])) {
                            $result .= "<!red>".$token[1]."<!>";
                        } else {
                            $result .= "<!grey>".$token[1]."<!>";
                        }
                    } else {
                        $result .= "<!grey>".$token."<!>";
                    }
            }
        }
        return $result;
    }

}
