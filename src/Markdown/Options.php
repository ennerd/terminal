<?php
namespace Charm\Terminal;

use Charm\Terminal;

class Options extends \Charm\AbstractOptions {

    const UTF_OFF = 0;
    const UTF_AUTO = 1;
    const UTF_ON = 2;

    /**
     * Use UTF line drawing characters and other UTF symbols to
     * improve style.
     */
    public int $utf = self::UTF_AUTO;

    /**
     * `Heading` <-- style for 'Heading'
     * `=======` <-- style for '======='
     */
    public string $h1_text = 'aqua italic';

    /**
     * `Heading`
     * `=======` <-- style for '======='
     */
    public string $h1_marker = 'white';


    /**
     * `Heading` <-- style for 'Heading'
     * `-------`
     */
    public string $h2_text = 'aqua italic';

    /**
     * `Heading`
     * `-------` <-- style for '======='
     */
    public string $h2_marker = 'white';

    /**
     * `# Heading` <-- style for 'Heading'
     */
    public string $hash_h1_text = 'aqua italic';

    /**
     * `# Heading` <-- style for '#'
     */
    public string $hash_h1_marker = 'aqua';

    /**
     * `## Heading` <-- style for 'Heading'
     */
    public string $hash_h2_text = 'aqua italic';

    /**
     * `## Heading` <-- style for '##'
     */
    public string $hash_h2_marker = 'aqua';

    /**
     * `### Heading` <-- style for 'Heading'
     */
    public string $hash_h3_text = 'aqua italic';

    /**
     * `### Heading` <-- style for '###'
     */
    public string $hash_h3_marker = 'aqua';

    /**
     * Inside code markers ``, code is highlighted with the following styles
     */
    public ?string $code_string_literal = 'yellow';      // strings and numbers
    public ?string $code_numeric_literal = 'red';      // strings and numbers
    public ?string $code_const_literal = 'red';      // strings and numbers
    public ?string $code_separator = 'white';   // parentheses, brackets, commans
    public ?string $code_comment = 'teal italic';      // comments in various styles
    public ?string $code_keyword = 'fuchsia';      // typical keywords and identifier
    public ?string $code_identifier = 'silver';   // variables, function names, namespaces etc
    public ?string $code_operator = 'red';   // plus, minus, multiplication etc
    public ?string $code_whitespace = null;     // any non-matched whitespace
    public ?string $code_unmatched = null;      // anything else that was not recognized

    /**
     * The column width to use (defaults to autodetect or 80).
     */
    public ?int $columns = null;

    protected function finalize() {
        if ($this->columns === null) {
            $this->columns = Terminal::getLines() ?? 80;
        }
    }
}
