<?php
namespace Charm\Terminal;

/**
 * Escape sequences for managing the terminal cursor
 */
class Cursor {

    public static function blink(bool $state) {
        return "\x1B[?12".($state ? "h" : "l");
    }

    public static function visible(bool $state) {
        return "\x1B[?25".($state ? "h" : "l");
    }

    public static function column(int $n) {
        return "\x1B[{$n}G";
    }

    public static function row(int $n) {
        return "\x1B[{$n}d";
    }

    public static function pos(int $x, int $y) {
        return "\x1B[{$y};{$x}f";
    }

    public static function save() {
        return "\x1B7";
    }

    public static function restore() {
        return "\x1B8";
    }

    public static function up(int $n=1) {
        return "\x1B[{$n}A";
    }

    public static function down(int $n=1) {
        return "\x1B[{$n}B";
    }

    public static function forward(int $n=1) {
        return $this->right($n;
    }

    public static function backward(int $n=1) {
        return $this->left($n;
    }

    public static function left(int $n=1) {
        return "\x1B[{$n}D";
    }

    public static function right(int $n=1) {
        return "\x1B[{$n}C";
    }
}
