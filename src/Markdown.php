<?php
namespace Charm\Terminal;

use Charm\Terminal;
use Charm\Terminal\Info\Char;

class Markdown {

    private $options;
    private $tokens;

    public function __construct(Options $options=null) {
        if ($options === null) {
            $options = new Options();
        }
        $this->options = $options;
    }

    public function write(string $markdown): string {
        preg_match_all(self::RE, $markdown, $matches, PREG_UNMATCHED_AS_NULL);
        $tokens = $matches[0];
        foreach ($matches as $key => $matches) {
            if (is_numeric($key)) {
                continue;
            }
            foreach (array_filter($matches) as $i => $match) {
                $tokens[$i] = [$key, $match];
            }
        }
        $result = '';
        foreach ($tokens as list($token, $content)) {
            if (method_exists($this, $token)) {
                $result .= $this->$token($content);
            } else {
                $result .= "CONTENT=$token\n".$content;
            }
        }
        return $result;
    }

    /**
     * # Heading
     *
     * or
     *
     * Heading
     * =======
     */
    private function h1(string $content) {
        $lines = explode("\n", $content);
        if (isset($lines[1]) && trim($lines[1], "=")==='') {
            // Heading
            // =======
            $length = Terminal::strlen($lines[1]);
            $lines[0] = "<!{$this->options->h1_text}>".$lines[0]."<!>";
            if ($this->options->utf) {
                $lines[1] = "<!{$this->options->h1_marker}>".str_repeat(Char::DOUBLE_BORDER[0], $length)."<!>";
            }
            return implode("\n", $lines);
        } else {
            return "<!{$this->options->hash_h1_marker}># <!><!{$this->options->hash_h1_text}>".Terminal::substr($content, 2)."<!>";
        }
    }
    /**
     * ## Heading
     *
     * or
     *
     * Heading
     * -------
     */
    private function h2(string $content) {
        $lines = explode("\n", $content);
        if (isset($lines[1]) && trim($lines[1], "=")==='') {
            $length = Terminal::strlen($lines[1]);
            $lines[0] = "<!{$this->options->h2_text}>".$lines[0]."<!>";
            if ($this->options->utf) {
                $lines[1] = "<!{$this->options->h2_marker}>".str_repeat(Char::DOUBLE_BORDER[0], $length)."<!>";
            }
            return implode("\n", $lines);
        } else {
            return "<!{$this->options->hash_h2_marker}># <!><!{$this->options->hash_h2_text}>".substr($content, 3)."<!>";
        }
    }

    /**
     * ### Heading
     */
    private function h3(string $content) {
        return "<!{$this->options->hash_h3_marker}>### <!><!{$this->options->hash_h3_text}>".substr($content, 4)."<!>";
    }

    private function empty(string $content) {
        return $content;
    }

    private function pre(string $content) {
        $tokens = self::tokenizeCode($content);

        $parts = []; // part 0 = initial whitespace, 1 = "```", 2 = 'php'/'js'/'' etc, 3 = [ tokens ], 4 = '```', 5 = trailing whitespace
        $tmp = '';
        foreach ($tokens as $token) {

            if (count($parts) === 0) {
                // eat whitespace until ```
                if ($token[1] !== '```') {
                    $tmp .= $token[1];
                } else {
                    $parts[] = $tmp;
                    $parts[] = $token[1];
                    $tmp = '';
                }
            } elseif (count($parts) === 2) {
                // capture until whitespace
                if ($token[0] !== 'whitespace') {
                    $tmp .= $token;
                } else {
                    $parts[] = $tmp;
                    $tmp = [ $token ];
                }
            } elseif (count($parts) === 3) {
                // capture tokens until '```'
                if ($token[1] !== '```') {
                    $tmp[] = $token;
                } else {
                    $parts[] = $tmp;
                    $tmp = $token[1];
                }
            } else {
                $tmp .= $token[1];
            }
        }

        $parts[] = $tmp;

        $result = $parts[0].$parts[1].$parts[2];

        $result .= $this->highlightCodeTokens($parts[3]);

        $result .= $parts[4];

        return $result;



        $lines = explode("\n", $content);
        $res = '';
        $l = $this->options->columns;
        foreach ($lines as $line) {
            $lineLength = mb_strlen(Terminal::unquote($line));
            if ($lineLength > $l) {
                $l = $lineLength;
            }
        }

        $isIn = false;
        foreach ($lines as $line) {
            $padExtra = strlen($line) - strlen(Terminal::unquote($line));
            if ($isIn === true && trim(substr($line, 0, 3)) === '```') {
                $isIn = false;
            }
            if ($isIn) {
                $line = "<!yellow>".Terminal::str_pad($line, $l + $padExtra)."<!>";
            }
            $res .= "$line\n";
            if ($isIn === false && trim(substr($line, 0, 3)) === '```') {
                $isIn = true;
            }
        }
        return $res;

        if ($this->options->utf) {
            $chars = Char::SINGLE_BORDER;
        } else {
            $chars = Char::SINGLE_BORDER_SIMPLE;
        }
        $res = implode("\n", $lines);
        $res = "```\n".self::prependLines($res, '    ')."\n```\n\n";

        return $res;
    }

    private static function prependLines(string $lines, string $prefix) {
        return $prefix.str_replace("\n", "\n".$prefix, $lines);
    }

    private function p(string $content) {
        $lines = explode("\n", $content);
        $combined = implode(" ", array_map('rtrim', $lines));
        return wordwrap($combined, $this->options->columns)."\n\n";
    }

    private function highlightInline(string $content) {
        
    }

    private function highlightCodeTokens(array $tokens) {
        $res = '';
        $map = [
            'strlit' => $this->options->code_string_literal,
            'numlit' => $this->options->code_numeric_literal,
            'constlit' => $this->options->code_const_literal,
            'separator' => $this->options->code_separator,
            'comment' => $this->options->code_comment,
            'keyword' => $this->options->code_keyword,
            'identifier' => $this->options->code_identifier,
            'operator' => $this->options->code_operator,
            'whitespace' => $this->options->code_whitespace,
            'unmatched' => $this->options->code_unmatched,
        ];
        $escape = function(string $s) {
            return strtr($res, [
                "\\\\" => "\\\\",
                "\\<!" => "\\\\<!",
            ]);
        };
        foreach ($tokens as $token) {
            if (!key_exists($token[0], $map)) {
                print_r($token);
                die("token type incorrect");
            }

            $style = $map[$token[0]];
            if ($style !== null) {
                if (substr($res, -1) === "\\") {
                    $res .= "\\";
                }
                $token[1] = '<!'.$style.'>'.$token[1].'<!>';
            } else {
                $token[1] = $token[1];
            }
            $res .= $token[1];
        }

echo $res."\n\n\n";


echo $res."\n";

        return $res;
    }

    private static function tokenizeInline(string $buffer) {
        $tokens = preg_match_all(<<<'RE'
            /
            /
            RE, $buffer, $matches);
        return self::regexToTokens($matches);
    }

    /**
     * Minimal and generic code tokenizer
     */
    private static function tokenizeCode(string $code) {
        $tokens = preg_match_all(<<<'RE'
            /
              (?<numlit>
                (0[bBxX][0-9a-zA-Z]++)
              | (0[0-7]++)
              | (\b(0|[1-9][0-9]*)(\.[0-9]++)?\b))
            | (?<strlit>
                ("(\\[\\"]|[^\\"]++|[^"])*+")
              | ('(\\[\\']|[^\\']++|[^'])*+')
              | (`(\\[\\`]|[^\\`]++|[^'])*+`)
              )
            | (?<constlit>(?i)true|false|null|undefined)
            | (?<separator>[{},;:()])
            | (?<comment>
                (\/\/[^\n]*+)
              | (\/\*([^*]*+|\*(?!\/))*\*\/)
              | (\#[^\n]*)
              )
            | (?<operator>[+\-*\/\\^:=<>&%!@?.]++|((?i)(and|or|not|xor)))
            | (?<keyword>
                await|break|case|catch|class|const|continue|debugger|default|delete|do|else|enum|
                export|extends|false|finally|for|function|if|implements|import|in|instanceof|
                interface|let|new|null|package|private|protected|public|return|super|switch|static|
                this|throw|try|true|typeof|var|void|while|with|yield|from|of|abstract|as|callable|
                u?int(8|16|32|64|128)?|float|binary|double|long|declare|echo|elseif|enddeclare|endfor|
                endforeach|endif|endswitch|endwhile|exit|final|fn|foreach|global|goto|match|namespace|
                readonly|trait|use|sizeof|auto|struct|enum|register|typedef|char|extern|union|
                signed|void|volatile|short|unsigned|
                \$?[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*+(?=\s++(\$?[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*+|[0-9]))
              )
            | (?<identifier>\$?[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*+)
            | (?<whitespace>\s++)
            | (?<symbol>(\S)+)
            | (?<unmatched>[\s\S])
            /mx
            RE, $code, $matches, PREG_SET_ORDER);
        return self::regexToTokens($matches);
    }

    private static function regexToTokens(string $matches) {
        $result = [];
        foreach ($matches as $match) {
            $kind = array_search($match[0], array_slice($match, 1, null, true), true);
            $result[] = [ $kind, $match[0] ];
        }
        return $result;
    }


    const RE = <<<'TOKENIZER'
        /
        (?<table>
          (
            (
              (^\h*\|[^\n]*\|\h*\n)
              |
              (^\|?\h*\S[^\v]*\|\h*\S[^\v]*\n)+
            )
            (^\h*(-++\||\|-++)[-|]*+\h*+\n)
            (
              (^\h*\|[^\n]*\|\h*\n)
              |
              (^\|?\h*\S[^\v]*\|\h*\S[^\v]*\n)+
            )
          )
        )
        |
        (?<quote>
            (^\h*>[^\n]*(\n|$))
            (
                ^\h*\S[^\n]*(\n|$)
            )*+
        )
        |
        (?<h3>
            ^\#\#\#(?>\h*\S[^\n]*)(\n|$)
        )
        |
        (?<h2>
            (^\#\#(?>\h*\S[^\n]*)(\n|$))
            |
            (^(?>\h*+\S[^\n]*\n)\h*-+\h*(\n|$))
        )
        |
        (?<h1>
            (^\#(?>\h*\S[^\n]*)(\n|$))
            |
            (^(?>\h*+\S[^\n]*\n)\h*=+\h*(\n|$))
        )
        |
        (?<ul>
            ^\h+[*-]\h+\S[^\n]*(\n|$)
        )
        |
        (?<ol>
            ^\h+(\#|\d+\.?)\h+\S[^\n]*(\n|$)
        )
        |
        (?<indent>
            ((?<ilin>^\h{4,})\S[^\n]*(\n|$))
            (
                (\h*\n){0,3}
                (\k<ilin>\S[^\n]+(\n|$))
            )*
        )
        |
        (?<pre>
            ^\h*```\w*\h*$
            (?>([^`]*|`{1,2}(?!`))*+)
            ^\h*```
        )
        |
        (?<hr>
            ^(\h*+---\h*+)(\n|$)
        )
        |
        (?<empty>
            ^[\h]*+(\n|$)
        )
        |
        (?<p>
            [\S\h]++(\n|$)
            (\h*\S[^\n]++(\n|$))*
        )
        /mx
        TOKENIZER;
}
