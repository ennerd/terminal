<?php
namespace Charm\Terminal;

class Display {

    public static function clear() {
        return "\x1B[2J\x1B[H";
    }

    public static function clearScreenAndBuffer() {
        return "\x1B[3J\x1B[H";
    }

    public static function clearStartOfLine() {
        return "\x1B[1K";
    }

    public static function clearEndOfLine() {
        return "\x1B[K";
    }

    public static function clearLine() {
        return "\x1B[2K";
    }

    public static function insertChar(int $n) {
        return "\x1B[{$n}@";
    }

    public static function deleteChar(int $n) {
        return "\x1B[{$n}P";
    }

    public static function eraseChar(int $n) {
        return "\x1B[{$n}X";
    }

    public static function insertLine(int $n) {
        return "\x1B[{$n}L";
    }

    public static function deleteLine(int $n) {
        return "\x1B[{$n}M";
    }

    public static function eraseInDisplay(string $textToErase) {
        return "\x1B[{$textToErase}J";
    }

    public static function scrollUp(int $n) {
        return "\x1B[{$n}S";
    }

    public static function scrollDown(int $n) {
        return "\x1B[{$n}T";
    }

}
