<?php
namespace Charm\Terminal\Info;

final class Color {

    public static function get($color): string {
        if (isset(self::NAMES[$color])) {
            $color = self::NAMES[$color];
            return "\x1B[38;5;".$color."m";
        } elseif (is_int($color)) {
            return "\x1B[38;5;".$color."m";
        } else {
            throw new \Exception("Expecting a known color name or a number");
        }
    }

    const NAMES = [
        'black' => self::BLACK,
        'maroon' => self::MAROON,
        'green' => self::GREEN,
        'olive' => self::OLIVE,
        'navy' => self::NAVY,
        'purple' => self::PURPLE,
        'teal' => self::TEAL,
        'silver' => self::SILVER,
        'grey' => self::GREY,
        'red' => self::RED,
        'lime' => self::LIME,
        'yellow' => self::YELLOW,
        'blue' => self::BLUE,
        'fuchsia' => self::FUCHSIA,
        'aqua' => self::AQUA,
        'white' => self::WHITE,
        'navyBlue' => self::NAVY_BLUE,
        'darkBlue' => self::DARK_BLUE,
        'darkGreen' => self::DARK_GREEN,
        'darkCyan' => self::DARK_CYAN,
        'lightSeaGreen' => self::LIGHT_SEA_GREEN,
        'darkTurquoise' => self::DARK_TURQUOISE,
        'mediumSpringGreen' => self::MEDIUM_SPRING_GREEN,
        'darkRed' => self::DARK_RED,
        'blueViolet' => self::BLUE_VIOLET,
        'steelBlue' => self::STEEL_BLUE,
        'cornflowerBlue' => self::CORNFLOWER_BLUE,
        'cadetBlue' => self::CADET_BLUE,
        'mediumTurquoise' => self::MEDIUM_TURQUOISE,
        'darkMagenta' => self::DARK_MAGENTA,
        'darkViolet' => self::DARK_VIOLET,
        'lightSlateGrey' => self::LIGHT_SLATE_GREY,
        'mediumPurple' => self::MEDIUM_PURPLE,
        'lightSlateBlue' => self::LIGHT_SLATE_BLUE,
        'darkSeaGreen' => self::DARK_SEA_GREEN,
        'lightGreen' => self::LIGHT_GREEN,
        'mediumVioletRed' => self::MEDIUM_VIOLET_RED,
        'indianRed' => self::INDIAN_RED,
        'mediumOrchid' => self::MEDIUM_ORCHID,
        'darkGoldenrod' => self::DARK_GOLDENROD,
        'rosyBrown' => self::ROSY_BROWN,
        'darkKhaki' => self::DARK_KHAKI,
        'lightSteelBlue' => self::LIGHT_STEEL_BLUE,
        'greenYellow' => self::GREEN_YELLOW,
        'orchid' => self::ORCHID,
        'violet' => self::VIOLET,
        'tan' => self::TAN,
        'hotPink' => self::HOT_PINK,
        'darkOrange' => self::DARK_ORANGE,
        'lightCoral' => self::LIGHT_CORAL,
        'sandyBrown' => self::SANDY_BROWN,
    ];

    // names as defined
    const BLACK = 0; // Black
    const MAROON = 1; // Maroon
    const GREEN = 2; // Green
    const OLIVE = 3; // Olive
    const NAVY = 4; // Navy
    const PURPLE = 5; // Purple
    const TEAL = 6; // Teal
    const SILVER = 7; // Silver
    const GREY = 8; // Grey
    const RED = 9; // Red
    const LIME = 10; // Lime
    const YELLOW = 11; // Yellow
    const BLUE = 12; // Blue
    const FUCHSIA = 13; // Fuchsia
    const AQUA = 14; // Aqua
    const WHITE = 15; // White
    const NAVY_BLUE = 17; // NavyBlue
    const DARK_BLUE = 18; // DarkBlue
    const DARK_GREEN = 22; // DarkGreen
    const DARK_CYAN = 36; // DarkCyan
    const LIGHT_SEA_GREEN = 37; // LightSeaGreen
    const DARK_TURQUOISE = 44; // DarkTurquoise
    const MEDIUM_SPRING_GREEN = 49; // MediumSpringGreen
    const DARK_RED = 52; // DarkRed
    const BLUE_VIOLET = 57; // BlueViolet
    const STEEL_BLUE = 67; // SteelBlue
    const CORNFLOWER_BLUE = 69; // CornflowerBlue
    const CADET_BLUE = 72; // CadetBlue
    const MEDIUM_TURQUOISE = 80; // MediumTurquoise
    const DARK_MAGENTA = 90; // DarkMagenta
    const DARK_VIOLET = 92; // DarkViolet
    const LIGHT_SLATE_GREY = 103; // LightSlateGrey
    const MEDIUM_PURPLE = 104; // MediumPurple
    const LIGHT_SLATE_BLUE = 105; // LightSlateBlue
    const DARK_SEA_GREEN = 108; // DarkSeaGreen
    const LIGHT_GREEN = 119; // LightGreen
    const MEDIUM_VIOLET_RED = 126; // MediumVioletRed
    const INDIAN_RED = 131; // IndianRed
    const MEDIUM_ORCHID = 134; // MediumOrchid
    const DARK_GOLDENROD = 136; // DarkGoldenrod
    const ROSY_BROWN = 138; // RosyBrown
    const DARK_KHAKI = 143; // DarkKhaki
    const LIGHT_STEEL_BLUE = 147; // LightSteelBlue
    const GREEN_YELLOW = 154; // GreenYellow
    const ORCHID = 170; // Orchid
    const VIOLET = 177; // Violet
    const TAN = 180; // Tan
    const HOT_PINK = 205; // HotPink
    const DARK_ORANGE = 208; // DarkOrange
    const LIGHT_CORAL = 210; // LightCoral
    const SANDY_BROWN = 215; // SandyBrown

	const XTERM_COLORS = [
        0x000000, // Black (SYSTEM)
        0x800000, // Maroon (SYSTEM)
        0x008000, // Green (SYSTEM)
        0x808000, // Olive (SYSTEM)
        0x000080, // Navy (SYSTEM)
        0x800080, // Purple (SYSTEM)
        0x008080, // Teal (SYSTEM)
        0xC0C0C0, // Silver (SYSTEM)
        0x808080, // Grey (SYSTEM)
        0xFF0000, // Red (SYSTEM)
        0x00FF00, // Lime (SYSTEM)
        0xFFFF00, // Yellow (SYSTEM)
        0x0000FF, // Blue (SYSTEM)
        0xFF00FF, // Fuchsia (SYSTEM)
        0x00FFFF, // Aqua (SYSTEM)
        0xFFFFFF, // White (SYSTEM)
        0x000000, // Grey0
        0x00005F, // NavyBlue
        0x000087, // DarkBlue
        0x0000AF, // Blue3
        0x0000D7, // Blue3
        0x0000FF, // Blue1
        0x005F00, // DarkGreen
        0x005F5F, // DeepSkyBlue4
        0x005F87, // DeepSkyBlue4
        0x005FAF, // DeepSkyBlue4
        0x005FD7, // DodgerBlue3
        0x005FFF, // DodgerBlue2
        0x008700, // Green4
        0x00875F, // SpringGreen4
        0x008787, // Turquoise4
        0x0087AF, // DeepSkyBlue3
        0x0087D7, // DeepSkyBlue3
        0x0087FF, // DodgerBlue1
        0x00AF00, // Green3
        0x00AF5F, // SpringGreen3
        0x00AF87, // DarkCyan
        0x00AFAF, // LightSeaGreen
        0x00AFD7, // DeepSkyBlue2
        0x00AFFF, // DeepSkyBlue1
        0x00D700, // Green3
        0x00D75F, // SpringGreen3
        0x00D787, // SpringGreen2
        0x00D7AF, // Cyan3
        0x00D7D7, // DarkTurquoise
        0x00D7FF, // Turquoise2
        0x00FF00, // Green1
        0x00FF5F, // SpringGreen2
        0x00FF87, // SpringGreen1
        0x00FFAF, // MediumSpringGreen
        0x00FFD7, // Cyan2
        0x00FFFF, // Cyan1
        0x5F0000, // DarkRed
        0x5F005F, // DeepPink4
        0x5F0087, // Purple4
        0x5F00AF, // Purple4
        0x5F00D7, // Purple3
        0x5F00FF, // BlueViolet
        0x5F5F00, // Orange4
        0x5F5F5F, // Grey37
        0x5F5F87, // MediumPurple4
        0x5F5FAF, // SlateBlue3
        0x5F5FD7, // SlateBlue3
        0x5F5FFF, // RoyalBlue1
        0x5F8700, // Chartreuse4
        0x5F875F, // DarkSeaGreen4
        0x5F8787, // PaleTurquoise4
        0x5F87AF, // SteelBlue
        0x5F87D7, // SteelBlue3
        0x5F87FF, // CornflowerBlue
        0x5FAF00, // Chartreuse3
        0x5FAF5F, // DarkSeaGreen4
        0x5FAF87, // CadetBlue
        0x5FAFAF, // CadetBlue
        0x5FAFD7, // SkyBlue3
        0x5FAFFF, // SteelBlue1
        0x5FD700, // Chartreuse3
        0x5FD75F, // PaleGreen3
        0x5FD787, // SeaGreen3
        0x5FD7AF, // Aquamarine3
        0x5FD7D7, // MediumTurquoise
        0x5FD7FF, // SteelBlue1
        0x5FFF00, // Chartreuse2
        0x5FFF5F, // SeaGreen2
        0x5FFF87, // SeaGreen1
        0x5FFFAF, // SeaGreen1
        0x5FFFD7, // Aquamarine1
        0x5FFFFF, // DarkSlateGray2
        0x870000, // DarkRed
        0x87005F, // DeepPink4
        0x870087, // DarkMagenta
        0x8700AF, // DarkMagenta
        0x8700D7, // DarkViolet
        0x8700FF, // Purple
        0x875F00, // Orange4
        0x875F5F, // LightPink4
        0x875F87, // Plum4
        0x875FAF, // MediumPurple3
        0x875FD7, // MediumPurple3
        0x875FFF, // SlateBlue1
        0x878700, // Yellow4
        0x87875F, // Wheat4
        0x878787, // Grey53
        0x8787AF, // LightSlateGrey
        0x8787D7, // MediumPurple
        0x8787FF, // LightSlateBlue
        0x87AF00, // Yellow4
        0x87AF5F, // DarkOliveGreen3
        0x87AF87, // DarkSeaGreen
        0x87AFAF, // LightSkyBlue3
        0x87AFD7, // LightSkyBlue3
        0x87AFFF, // SkyBlue2
        0x87D700, // Chartreuse2
        0x87D75F, // DarkOliveGreen3
        0x87D787, // PaleGreen3
        0x87D7AF, // DarkSeaGreen3
        0x87D7D7, // DarkSlateGray3
        0x87D7FF, // SkyBlue1
        0x87FF00, // Chartreuse1
        0x87FF5F, // LightGreen
        0x87FF87, // LightGreen
        0x87FFAF, // PaleGreen1
        0x87FFD7, // Aquamarine1
        0x87FFFF, // DarkSlateGray1
        0xAF0000, // Red3
        0xAF005F, // DeepPink4
        0xAF0087, // MediumVioletRed
        0xAF00AF, // Magenta3
        0xAF00D7, // DarkViolet
        0xAF00FF, // Purple
        0xAF5F00, // DarkOrange3
        0xAF5F5F, // IndianRed
        0xAF5F87, // HotPink3
        0xAF5FAF, // MediumOrchid3
        0xAF5FD7, // MediumOrchid
        0xAF5FFF, // MediumPurple2
        0xAF8700, // DarkGoldenrod
        0xAF875F, // LightSalmon3
        0xAF8787, // RosyBrown
        0xAF87AF, // Grey63
        0xAF87D7, // MediumPurple2
        0xAF87FF, // MediumPurple1
        0xAFAF00, // Gold3
        0xAFAF5F, // DarkKhaki
        0xAFAF87, // NavajoWhite3
        0xAFAFAF, // Grey69
        0xAFAFD7, // LightSteelBlue3
        0xAFAFFF, // LightSteelBlue
        0xAFD700, // Yellow3
        0xAFD75F, // DarkOliveGreen3
        0xAFD787, // DarkSeaGreen3
        0xAFD7AF, // DarkSeaGreen2
        0xAFD7D7, // LightCyan3
        0xAFD7FF, // LightSkyBlue1
        0xAFFF00, // GreenYellow
        0xAFFF5F, // DarkOliveGreen2
        0xAFFF87, // PaleGreen1
        0xAFFFAF, // DarkSeaGreen2
        0xAFFFD7, // DarkSeaGreen1
        0xAFFFFF, // PaleTurquoise1
        0xD70000, // Red3
        0xD7005F, // DeepPink3
        0xD70087, // DeepPink3
        0xD700AF, // Magenta3
        0xD700D7, // Magenta3
        0xD700FF, // Magenta2
        0xD75F00, // DarkOrange3
        0xD75F5F, // IndianRed
        0xD75F87, // HotPink3
        0xD75FAF, // HotPink2
        0xD75FD7, // Orchid
        0xD75FFF, // MediumOrchid1
        0xD78700, // Orange3
        0xD7875F, // LightSalmon3
        0xD78787, // LightPink3
        0xD787AF, // Pink3
        0xD787D7, // Plum3
        0xD787FF, // Violet
        0xD7AF00, // Gold3
        0xD7AF5F, // LightGoldenrod3
        0xD7AF87, // Tan
        0xD7AFAF, // MistyRose3
        0xD7AFD7, // Thistle3
        0xD7AFFF, // Plum2
        0xD7D700, // Yellow3
        0xD7D75F, // Khaki3
        0xD7D787, // LightGoldenrod2
        0xD7D7AF, // LightYellow3
        0xD7D7D7, // Grey84
        0xD7D7FF, // LightSteelBlue1
        0xD7FF00, // Yellow2
        0xD7FF5F, // DarkOliveGreen1
        0xD7FF87, // DarkOliveGreen1
        0xD7FFAF, // DarkSeaGreen1
        0xD7FFD7, // Honeydew2
        0xD7FFFF, // LightCyan1
        0xFF0000, // Red1
        0xFF005F, // DeepPink2
        0xFF0087, // DeepPink1
        0xFF00AF, // DeepPink1
        0xFF00D7, // Magenta2
        0xFF00FF, // Magenta1
        0xFF5F00, // OrangeRed1
        0xFF5F5F, // IndianRed1
        0xFF5F87, // IndianRed1
        0xFF5FAF, // HotPink
        0xFF5FD7, // HotPink
        0xFF5FFF, // MediumOrchid1
        0xFF8700, // DarkOrange
        0xFF875F, // Salmon1
        0xFF8787, // LightCoral
        0xFF87AF, // PaleVioletRed1
        0xFF87D7, // Orchid2
        0xFF87FF, // Orchid1
        0xFFAF00, // Orange1
        0xFFAF5F, // SandyBrown
        0xFFAF87, // LightSalmon1
        0xFFAFAF, // LightPink1
        0xFFAFD7, // Pink1
        0xFFAFFF, // Plum1
        0xFFD700, // Gold1
        0xFFD75F, // LightGoldenrod2
        0xFFD787, // LightGoldenrod2
        0xFFD7AF, // NavajoWhite1
        0xFFD7D7, // MistyRose1
        0xFFD7FF, // Thistle1
        0xFFFF00, // Yellow1
        0xFFFF5F, // LightGoldenrod1
        0xFFFF87, // Khaki1
        0xFFFFAF, // Wheat1
        0xFFFFD7, // Cornsilk1
        0xFFFFFF, // Grey100
        0x080808, // Grey3
        0x121212, // Grey7
        0x1C1C1C, // Grey11
        0x262626, // Grey15
        0x303030, // Grey19
        0x3A3A3A, // Grey23
        0x444444, // Grey27
        0x4E4E4E, // Grey30
        0x585858, // Grey35
        0x626262, // Grey39
        0x6C6C6C, // Grey42
        0x767676, // Grey46
        0x808080, // Grey50
        0x8A8A8A, // Grey54
        0x949494, // Grey58
        0x9E9E9E, // Grey62
        0xA8A8A8, // Grey66
        0xB2B2B2, // Grey70
        0xBCBCBC, // Grey74
        0xC6C6C6, // Grey78
        0xD0D0D0, // Grey82
        0xDADADA, // Grey85
        0xE4E4E4, // Grey89
        0xEEEEEE, // Grey93
    ];
}
