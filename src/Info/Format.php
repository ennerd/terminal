<?php
namespace Charm\Terminal\Info;

final class Format {

    const NAMES = [
        'bold' => self::BOLD,
        'italic' => self::ITALIC,
        'underline' => self::UNDERLINE,
        'strikeThrough' => self::STRIKE_THROUGH,
        'negative' => self::NEGATIVE,
    ];

    /**
     * Indicates the code for enabling and disabling each formatting
     * code
     */
    const BOLD = [1, 22];
    const ITALIC = [3, 23];
    const UNDERLINE = [4, 24];
    const NEGATIVE = [7, 27];
    const STRIKE_THROUGH = [9, 29];

}
