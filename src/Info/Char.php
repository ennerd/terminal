<?php
namespace Charm\Terminal\Info;

/**
 * This interface organizes a lot of useful UTF-8 characters to help working
 * with the terminal.
 */
final class Char {

    /**
     * Graphics characters for progress bars and bar charts.
     * All characters are adressed from number 0 to 7.
     */
    const VERTICAL_BAR              = ['▁','▂','▃','▄','▅','▆','▇','█'];       // 0-7
    const VERTICAL_BAR_REVERSED     = ['▔','▔','▔','▀','▀','▀','█','█'];       // 0-7
    const HORIZONTAL_BAR            = ['▏','▎','▍','▌','▋','▊','▉','█'];       // 0-7
    const HORIZONTAL_BAR_REVERSED   = ['█','▉','▊','▋','▌','▍','▎','▏'];       // 0-7

    /**
     * Dotted line horizontal and vertical
     */
    const DOTTED_LINE = ['╌','┆'];

    const SINGLE_BORDER_PLAIN = [
        '-',
        '+', '+', '+',
        '+', '+', '+',
        '+', '+', '+',
        '|'];

    const DOUBLE_BORDER_PLAIN = [
        '=',
        '+', '+', '+',
        '+', '+', '+',
        '+', '+', '+',
        '|'];

    /**
     * "Number pad layout"
     *
     * Coordinates for the following graphics match the numeric keyboard
     * characters 1-9, so 7 is the top left, 3 is the bottom right. For
     * example Terminal::SINGLE_BORDER[5] is the center cross.
     *
     * 0 is mapped to the horizontal line, and 10 is mapped to the vertical line
     *
     *  ┌┬┐     789
     *  ├┼┤     456 BORDER[0][1] gives you ├
     *  └┴┘     123
     *  ─ │     0 10
     */
    const SINGLE_BORDER = [
        '─',
        '└','┴','┘',
        '├','┼','┤',
        '┌','┬','┐',
        '│'];

    /**
     *  ╔╦╗
     *  ╠╬╣
     *  ╚╩╝
     *  ═ ║
     */
    const DOUBLE_BORDER = [
        '═',
        '╚','╩','╝',
        '╠','╬','╣',
        '╔','╦','╗',
        '║'];


    /**
     *  ╓╥╖
     *  ╟╫╢
     *  ╙╨╜
     *  ─ ║
     */
    const VDOUBLE_BORDER = [
        '─',
        '╓','╥','╖',
        '╟','╫','╢',
        '╙','╨','╜',
        '║'];

    /**
     *  ╒╤╕
     *  ╞╪╡
     *  ╘╧╛
     *  ═ │
     */
    const HDOUBLE_BORDER = [
        '═',
        '╘','╧','╛',
        '╞','╪','╡',
        '╒','╤','╕',
        '│'];

    /**
     *  ╭┬╮
     *  ├┼┤
     *  ╰┴╯
     *  ─ │
     */
    const ROUND_BORDER = [
        '─',
        '╰','┴','╯',
        '├','┼','┤',
        '╭','┬','╮',
        '│'];

    /**
     * Diagonal line from top-left to bottom-right, top-right to bottom-left
     * and a cross.
     */
    const DIAGONALS = [
        '╲','╱','╳'
        ];

    /**
     * Half length lines
     */
    const ENDS = [
        '╵','╶','╷','╴',
        '╹','╺','╻','╸'
        ];

    /**
     * Characters to represent ASCII control characters 0 to 39.
     */
    const CONTROL_CHARACTERS = [
        '␀','␁','␂','␃','␄','␅','␆','␇',
        '␈','␉','␊','␋','␌','␍','␎','␏',
        '␐','␑','␒','␓','␔','␕','␖','␗',
        '␘','␙','␚','␛','␜','␝','␞','␟',
        '␠','␡','␢','␣','␤','␥','␦',
        ];

    /**
     * Characters that represent four corners of a character as a bit which is on or off.
     * The mapping is such that the first bit represents top left, second bit represents top
     * right and so on.
     */
    const BITPIXELS = [
        0b0000 => ' ',
        0b0001 => '▗',
        0b0010 => '▖',
        0b0011 => '▄',
        0b0100 => '▝',
        0b0101 => '▐',
        0b0110 => '▞',
        0b0111 => '▟',
        0b1000 => '▘',
        0b1001 => '▚',
        0b1010 => '▌',
        0b1011 => '▙',
        0b1100 => '▀',
        0b1101 => '▜',
        0b1110 => '▛',
        0b1111 => '█'];

    /**
     * Arrows point in the direction of the numbers on the number pad
     *   ↖↑↗    789
     *   ← →    456
     *   ↙↓↘    123
     *   ↔ ↕    0 10
     */

    const ARROW = [
        '↔',
        '↙','↓','↘',
        '←','↔','→',
        '↖','↑','↗',
        '↕',
        ];

    /**
     *  ⇖⇑⇗     789
     *  ⇐ ⇒     456
     *  ⇙⇓⇘     123
     *  ⇔ ⇕     0 10
     */
    const DOUBLE_ARROW = [
        '⇔',
        '⇖','⇑','⇗',
        '⇐',' ','⇒',
        '⇙','⇓','⇘',
        '⇕'];

    /**
     * Numeric fractions mapping
     */
    const FRACTIONS = [
        0 => [
            3 => '↉',
            ],
        1 => [
            3 => '⅓',
            5 => '⅕',
            6 => '⅙',
            7 => '⅐',
            9 => '⅑',
            10 => '⅒',
            ],
        2 => [
            3 => '⅔',
            5 => '⅖',
            ],
        3 => [
            5 => '⅗',
            8 => '⅜',
            ],
        4 => [
            5 => '⅘',
            ],
        5 => [
            6 => '⅚',
            8 => '⅝',
            ],
        7 => [
            8 => '⅞',
            ],
        ];

    const SUPERSCRIPT = [
        '0' => '⁰',
        '1' => '¹',
        '2' => '²',
        '3' => '³',
        '4' => '⁴',
        '5' => '⁵',
        '6' => '⁶',
        '7' => '⁷',
        '8' => '⁸',
        '9' => '⁹'
        ];

    const SUBSCRIPT = [
        '0' => '₀',
        '1' => '₁',
        '2' => '₂',
        '3' => '₃',
        '4' => '₄',
        '5' => '₅',
        '6' => '₆',
        '7' => '₇',
        '8' => '₈',
        '9' => '₉'
        ];

    const SPECIAL = [
        'ellipsis' => '…',
        'qoute' => [
            ['‘', '’'],
            ['“', '”'],
            ['‚', '‛'],
            ['„', '‟'],
            ['‹', '›'],
            ],
        ];

    const GRADIENT = [
        ' ','░','▒','▓','█'
        ];

    const CAKE = [
        '○','◔','◑','◕'
        ];

    const CHECK = [
        '✘', '✓',
        ];
}
