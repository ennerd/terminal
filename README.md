Charm/Terminal
==============

**`Charm\Terminal` is a class for writing to streams while parsing the markup language.**

Generate nice and helpful output for the command line with minimal extra work, by adding a
very simple markup language to your output. This greatly simplifies working with colors in
the terminal:

 1. The tags work like HTML, except their opening tag is `<!TAG-NAME>` and their closing
    tag is `<!>`.
 2. Multiple tags can be combined into one with `<!yellow underline>example<!>`.
 3. Tags can be nested.
 4. When the client is not using a TTY, no CSI codes will be sent - which means that your
    output will have colors only when used interactively.
 5. Tags can be escaped with backslash, like `\<!this>`


## Example

```
$term = new Charm\Terminal(STDERR);

// Clear the screen
$term->write('<!clear>');

// Write "Hello World"
$term->write('<!bwhite>Hello<!> <!underline>World<!>\n');
```

## Description

```
(new Charm\Terminal($stream))->write($output);
```

Writes the contents of *output* to the file stream pointed to by *stream*.


## Parameters

### stream

A file system pointer resource that is typically the PHP constants `STDERR` or `STDOUT`.

### output

The text with markup that you wish to print.


## Return values

Returns nothing


## Tag reference

|---------------|-----------------------------------------------------------------|
| Tag           | Description                                                     |
|---------------|-----------------------------------------------------------------|
| bold          | Makes text **bold**                                             |
| italic        | Makes text *italic*                                             |
| underline     | Makes test <u>underlined</u>                                    |
| strikeThrough | Makes text ~~strike through~~                                   |
| blink         | Makes text blink slowly                                         |
| fastblink     | Makes text blink quickly                                        |
| clear         | Clear the entire terminal window                                |
| home          | Move cursor to line 1, column 1                                 |
| clear-line    | Clears the entire line at the cursor                            |
| clear-left    | Clears the line to the left of the cursor                       |
| clear-right   | Clears the line to the right of the cursor                      |
| pos(L, C)     | Move cursor to line *L*, column *C*                             |
| negative      | Invert foreground and background color                          |
|---------------|-----------------------------------------------------------------|


## Color reference

|------------------|----------------|---------------------------------------------|
| Standard color   | Bright color   | Description                                 |
|------------------|----------------|---------------------------------------------|
| black            | grey           | Black and grey                              |
| silver           | white          | White and bright white                      |
| maroon           | red            | Red and bright red                          |
| green            | lime           | Green and bright green                      |
| olive            | yellow         | Yellow and bright yellow                    |
| navy             | blue           | Blue and bright blue                        |
| puple            | fuchsia        | Purple and bright purple                    |
| teal             | aqua           | Teal and bright teal                        |
| silver           | white          | White and bright white                      |
|------------------|----------------|---------------------------------------------|

> Note! A full list of supported colors can be found in `src/Info/Color.php`.


Background colors can be set by suffixing the color with `BG` or `Background`:

```
$term->write("<!redBG white> This is white on red <!>");
```


## Examples

### Example #1 object oriented usage

```
$terminal = new Charm\Terminal(STDOUT);
$terminal->write("<!red>This text is <!white underline>WHITE WITH UNDERLINE<!> wrapped in red<!>");
```

### Example #2 function usage

```
Charm\term_write(STDOUT, "<!red>This will be <!underline green>green underlined<!!>");
```


## Notes

The tag `<!>` will close the previous tag. If you want to close 3 tags at once use `<!!!>`.

