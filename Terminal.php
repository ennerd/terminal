<?php
namespace Charm;

use Charm\Terminal\Info\{
    Color,
    Format
};

/**
 * Terminal output processor. Add foreground and background colors easily. Manipulate
 * strings containing color tags. Remove colors if the output is not a TTY.
 */
class Terminal {

    const DEFAULT_STATE = [
        'suppress_output' => false,                             // silence will prevent all output
        'color' => '39', //;5;'.Color::SILVER,
        'background' => '49',
        'bold' => Format::BOLD[1],
        'italic' => Format::ITALIC[1],
        'underline' => Format::UNDERLINE[1],
        'strikeThrough' => Format::STRIKE_THROUGH[1],
        'negative' => Format::NEGATIVE[1],
    ];

    public static $defaultState = self::DEFAULT_STATE;

    private $stream;
    public $isTTY;
    private $state = [];
    private $stateOffset = 0;
    private array $parsers = [];

    // holds a custom parser/highlighter when it is activated
    private ?array $parser = null;

    public function __construct($stream) {
        $this->options = $options ?? new Terminal\Options();
        $this->stream = $stream;
        $this->isTTY =
            (function_exists('stream_isatty') && stream_isatty($stream)) ||
            (function_exists('posix_isatty') && posix_isatty($stream)) ||
            false;

        $this->parsers['php'] = function() {
            return new Terminal\Parser\PhpHighlighter($this);
        };
    }

    /**
     * Get the width of the terminal window.
     */
    public static function getCols(): int {
        $tputCols = (int) (exec('tput cols') ?? 0);
        if ($tputCols > 0) {
            return $tputCols;
        }
        return 80;
    }

    /**
     * Try to get the line count from the terminal
     */
    public static function getLines(): int {
        $tputLines = (int) (exec('tput lines') ?? 0);
        if ($tputLines > 0) {
            return $tputLines;
        }
        return 50;
    }

    public function __destruct() {
        if ($this->stateOffset > 0) {
            $this->write($this->sendState(self::$defaultState));
            $this->stateOffset = 0;
        }
    }

    private static function tokenize(string $bytes): array {
        preg_match_all(<<<'RE'
            /(?<text>(\\[\\[\\<]|[^<])++)|(?<token><!+([-_a-zA-Z0-9](\(([0-9+]\ ?)+\))?\ *)+>)|(?<close><!+>)|[\s\S]/
            RE, $bytes, $matches, PREG_SET_ORDER);

        $res = [];
        foreach ($matches as $match) {
            if (!empty($match['token']) || !empty($match['close'])) {
                $res[] = ['token', $match[0]];
            } else {
                $res[] = ['text', $match[0]];
            }
        }
        return $res;
    }

    public function write(string $bytes) {
        if ($this->isTTY) {
            return fwrite($this->stream, $this->process($bytes));
        } else {
            return fwrite($this->stream, $this->strip($bytes));
        }
        return $this;
    }

    public static function strip(string $bytes) {
        $res = '';

        foreach (self::tokenize($bytes) as $token) {
            if ($token[0] === 'text') {
                $res .= $token[1];
            }
        }

        return $res;
    }

    public function process(string $bytes) {
        $res = '';

        foreach ($this->tokenize($bytes) as $token) {

            if ($this->parser !== null) {
                if ($token[0] === 'token' && $token[1] === '<!>') {
                    $parser = $this->parser;
                    $this->parser = null;
                    $buffer = $parser[0];
                    $res .= $this->process(($parser[1])($buffer));
                    continue;
                } else {
                    $this->parser[0] .= $token[1];
                    continue;
                }
            }

            if ($this->stateOffset === 0) {
                $state = static::$defaultState;
            } else {
                $state = $this->state[$this->stateOffset-1];
            }
            if ($token[0] === 'text') {
                if (!$state['suppress_output']) {
                    $res .= $token[1];
                }
            } elseif (trim($token[1], '<!>') === '') {
                // pop statement <!>
                for ($i = 2; $i < strlen($token[1]); $i++) {
                    if ($this->stateOffset === 0) {
                        throw new \Exception("Mismatching statement '".$token[1]."' in ".substr($bytes, 0, 40).".");
                    }
                    --$this->stateOffset;
                }
                if ($this->stateOffset === 0) {
                    $res .= $this->sendState(self::$defaultState);
                } else {
                    $res .= $this->sendState($this->state[$this->stateOffset-1]);
                }
            } else {
                if ($this->stateOffset === 0) {
                    $state = static::$defaultState;
                } else {
                    $state = $this->state[$this->stateOffset-1];
                }
                preg_match_all('/([-_a-zA-Z0-9]+)|\([-_a-zA-Z0-9 ]+\)|\s+/', trim(substr($token[1], 2, -1)), $instructionMatches);
                $instructions = [];
                $instructionCount = 0;
                for ($i = 0; $i < count($instructionMatches[0]); $i++){
                    $im = $instructionMatches[0][$i];
                    if (trim($im) === '') {
                        continue;
                    }
                    if ($im[0] === '(') {
                        if ($instructionCount === 0 || is_array($instructions[$instructionCount-1])) {
                            throw new \Exception("Unexpected `$im` in instruction `".$token[1]."`");
                        }
                        $instructions[$instructionCount-1] = [ $instructions[$instructionCount-1] ];
                        foreach (preg_split('/\s+/', substr($im, 1, -1)) as $part) {
                            if (is_numeric($part)) {
                                $part = floatval($part);
                            }
                            $instructions[$instructionCount-1][] = $part;
                        }
                        continue;
                    }
                    $instructions[$instructionCount++] = $im;
                }

                $changedState = false;
                foreach ($instructions as $instruction) {
                    if (is_string($instruction)) {
                        $instruction = [ $instruction ];
                    }
                    // instructions without arguments
                    if (isset(Color::NAMES[$instruction[0]])) {
                        $changedState = true;
                        $state['color'] = '38;5;'.Color::NAMES[$instruction[0]];
                    } elseif (isset(Format::NAMES[$instruction[0]])) {
                        $changedState = true;
                        $state[$instruction[0]] = Format::NAMES[$instruction[0]][0];
                    } elseif (
                        (
                            substr($instruction[0], -2) === 'BG' && 
                            isset(Color::NAMES[$bgColor = substr($instruction[0], 0, -2)]
                        ) ||
                        (
                            substr($instruction[0], -10) === 'Background') && 
                            isset(Color::NAMES[$bgColor = substr($instruction[0], 0, -10)])
                        )
                    ) {
                        $changedState = true;
                        $state['background'] = '48;5;'.Color::NAMES[$bgColor];
                    } elseif (isset($this->parsers[$instruction[0]])) {
                        $parserInstance = ($this->parsers[$instruction[0]])($this);
                        $this->parser = [ '', $parserInstance ];
                    } else {
                        switch ($instruction[0]) {
                            case 'tty' :
                                if (!$this->isTTY) {
                                    $state['suppress_output'] = true;
                                }
                                $changedState = true;
                                break;
                            case 'no-tty' :
                                if ($this->isTTY) {
                                    $state['suppress_output'] = true;
                                }
                                $changedState = true;
                                break;
                            case 'clear-line' : $res .= $this->clearLine(); break;
                            case 'clear-start' :
                            case 'clear-left' : $res .= $this->clearLeft(); break;
                            case 'clear-end' :
                            case 'clear-right' : $res .= $this->clearRight(); break;
                            case 'clear' : $res .= $this->clear(); break;
                            case 'home' : $res .= $this->pos(1,1); break;
                            case 'pos' :
                                if (count($instruction) !== 3) {
                                    throw new \Exception("pos requires 2 arguments, for example `<!pos(20 10)>`");
                                }
                                $res .= $this->pos($instruction[1], $instruction[2]);
                                break;
                            default :
                                throw new \Exception("Unknown instruction `{$instruction[0]}` in `".$token[1]."`");
                        }
                    }
                }
                if ($changedState) {
                    $this->state[$this->stateOffset++] = $state;
                    $res .= $this->sendState($state);
                }
            }
        }
        return self::unquote($res);
    }

    public static function strlen(string $buffer) {
        return mb_strlen(self::strip($buffer));
    }

    public static function substr(string $buffer, int $start, int $length=100000) {

    }

    public static function str_pad(string $buffer, int $length, string $pad_string=' ', int $pad_type = STR_PAD_RIGHT) {
        $stripped = self::strip($buffer);
        $strippedLength = mb_strlen($stripped);
        $currentLength = mb_strlen($buffer);
        $diff = $currentLength - $strippedLength;
        $length += $diff;
        return str_pad($buffer, $length, $pad_string, $pad_type);
    }

    public static function quote(string $buffer): string {
        return strtr($buffer, [
            "\\" => "\\\\",
            "<" => "\\<",
        ]);
        $res = '';
        foreach (self::tokenize($buffer) as $token) {
            if ($token[0] === 'text') {
                $res .= $token[1];
            } else {
                $res .= "\\".$token[1];
            }
        }
        return $res;
    }

    public static function unquote(string $buffer): string {
        return strtr($buffer, [
            "\\\\" => "\\",
            "\\<" => "<",
        ]);
    }

    private function sendState(array $state): string {
        return "\x1B[".implode(";", $state)."m";
    }

    private function writeIfTTY(string $bytes) {
        if (!$this->isTTY()) {
            return $this;
        }
        return $this->write($bytes);
    }

    public function isTTY(): bool {
        return $this->isTTY;
    }

    public function reset() {
        return "\x1B[0m";
    }

    public function clear() {
        return "\x1B[2J".$this->home();
    }

    public function home() {
        return "\x1B[1;1H";
    }

    public function clearStart() {
        return $this->clearLeft();
    }

    public function clearLeft() {
        return "\x1B[1K";
    }

    public function clearEnd() {
        return $this->clearRight();
    }

    public function clearRight() {
        return "\x1B[K";
    }

    public function clearLine() {
        return "\x1B[2K";
    }

    public function insertChar(int $n) {
        return "\x1B[{$n}@";
    }

    public function deleteChar(int $n) {
        return "\x1B[{$n}P";
    }

    public function eraseChar(int $n) {
        return "\x1B[{$n}X";
    }

    public function insertLine(int $n) {
        return "\x1B[{$n}L";
    }

    public function deleteLine(int $n) {
        return "\x1B[{$n}M";
    }

    public function eraseInDisplay(string $textToErase) {
        return "\x1B[{$textToErase}J";
    }

    public function scrollUp(int $n) {
        return "\x1B[{$n}S";
    }

    public function scrollDown(int $n) {
        return "\x1B[{$n}T";
    }

    public function saveCursor() {
        return "\x1B7";
    }

    public function restoreCursor() {
        return "\x1B8";
    }

    public function up(int $n=1) {
        return "\x1B[{$n}A";
    }

    public function down(int $n=1) {
        return "\x1B[{$n}B";
    }

    public function forward(int $n=1) {
        return $this->right($n);
    }

    public function backward(int $n=1) {
        return $this->left($n);
    }

    public function left(int $n=1) {
        return "\x1B[{$n}D";
    }

    public function right(int $n=1) {
        return "\x1B[{$n}C";
    }

    public function column(int $n) {
        return "\x1B[{$n}G";
    }

    public function row(int $n) {
        return "\x1B[{$n}d";
    }

    public function pos(int $x, int $y) {
        return "\x1B[{$y};{$x}f";
    }

    public function cursorBlink(bool $state) {
        return "\x1B[?12".($state ? "h" : "l");
    }

    public function cursorVisible(bool $state) {
        return "\x1B[?25".($state ? "h" : "l");
    }

}
